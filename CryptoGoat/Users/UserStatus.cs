﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoGoat.Users
{
    public enum UserStatus
    {
        NoAttempt,
        HandshakeInitiated,
        Connected,
        Closed
    }
}
