﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Net;
using System.Net.Sockets;
using CryptoGoat.Com;

namespace CryptoGoat.Users
{
    public abstract class Connection
    {
        private string name;
        private IPAddress ipAddress;
        private int port;
        private IPEndPoint endpoint;
        private CommunicationManager communicationManager;
        private Queue<Message> messages = new Queue<Message>();

        private UserStatus status = UserStatus.NoAttempt;
        public UserStatus Status
        {
            get { return status; }
        }

        public string Name
        {
            get { return name; }
        }

        public IPAddress IPAddress
        {
            get { return this.ipAddress; }
        }

        public IPEndPoint Endpoint
        {
            get { return this.endpoint; }
        }

        public int Port
        {
            get { return this.port; }
        }

        public Connection(string name, IPAddress ip, int port)
        {
            this.name = name;
            this.ipAddress = ip;
            this.port = port;
            endpoint = new IPEndPoint(ip, port);
        }

        public void setManager(CommunicationManager manager)
        {
            this.communicationManager = manager;
        }

        public void Send(Message text)
        {
            byte[] data = text.GetBytes();
            this.communicationManager.Send(data, this);
        }

        public void InitializeHandshake()
        {
            HandshakeInitMessage connMessage = new HandshakeInitMessage();
            this.status = UserStatus.HandshakeInitiated;
            this.Send(connMessage);
        }

        public void AddMessage(Message message)
        {
            if(this.status == UserStatus.NoAttempt)
            {
                InitializeHandshake();
            }
            else if(this.status == UserStatus.HandshakeInitiated)
            {
                Console.WriteLine("CONNECTED: " + this.endpoint.Address.ToString() + " " + this.endpoint.Address.ToString() + " " + this.endpoint.Port);

            }
        }
    }
}
