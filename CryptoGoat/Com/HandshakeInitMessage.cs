﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoGoat.Com
{
    public class HandshakeInitMessage : ConnectionMessage
    {
        public HandshakeInitMessage()
        {
            this.raw += "Connection INIT";
        }
    }
}
