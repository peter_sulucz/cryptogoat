﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoGoat.Com
{
    public class ReceiveMessage : Message
    {
        public ReceiveMessage(byte[] data)
        {
            this.raw = ASCIIEncoding.ASCII.GetString(data);
        }
    }
}
