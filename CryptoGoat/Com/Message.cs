﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoGoat.Com
{
    public class Message
    {
        protected string raw;
        public string Raw
        {
            get { return raw; }
        }

        protected int messageType;
        public int MessageType
        {
            get { return messageType; }
        }

        public virtual byte[] GetBytes()
        {
            return ASCIIEncoding.ASCII.GetBytes(this.messageType + this.raw);
        }
    }
}
