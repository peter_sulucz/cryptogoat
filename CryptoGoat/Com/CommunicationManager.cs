﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.ObjectModel;
using CryptoGoat.Users;

namespace CryptoGoat.Com
{
    public class CommunicationManager
    {
        private int port = 5432;
        private Thread listenerThread;

        private Socket udpSocket;
        private Socket connectionListener;

        private List<Connection> users;

        public List<Message> messages;

        private bool listenerActive;

        public CommunicationManager()
        {
            this.messages = new List<Message>();
            this.users = new List<Connection>();
        }

        public delegate void NewMessageEvent(Message message);

        public event NewMessageEvent OnNewMessage;

        public delegate void NewUserEvent(Connection user);
        public event NewUserEvent OnNewUser;
        
        public void Start(int port)
        {
            this.port = port;
            // listener
            this.listenerThread = new Thread(new ThreadStart(ListenerTask));
            this.listenerActive = true;

            //this.udpSocket = new Socket(SocketType.Dgram, ProtocolType.Udp);
            this.connectionListener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //this.listenerThread.SetApartmentState(ApartmentState.STA);
            this.listenerThread.Start();
        }

        public void AddUser(Connection user)
        {
            this.users.Add(user);
            user.setManager(this);

            user.InitializeHandshake();
            //ConnectionMessage message = new ConnectionMessage(this.port, "testname");
            //user.Send(message);
            //OutgoingConnection connection = new OutgoingConnection(user.IPAddress, user.Port);
            //ThreadPool.QueueUserWorkItem(connection.CreateConnection);

            if (this.OnNewMessage != null)
                this.OnNewUser(user);
        }

        public void Send(byte[] data, Connection user)
        {
            this.connectionListener.SendTo(data, user.Endpoint);
        }

        public void BroadcastAll(byte[] data)
        {
            foreach(Connection u in this.users)
            {
                this.Send(data, u);
            }
        }

        
        public void ListenerTask()
        {
            IPEndPoint listnerpoint = new IPEndPoint(IPAddress.Any, port);
            //this.udpSocket.Bind(listnerpoint);
            byte[] buffer = new byte[2048];
            //List<ArraySegment<byte>> reclist = new List<ArraySegment<byte>>();
            this.connectionListener.Bind(listnerpoint);
            
            while(this.listenerActive)
            {
                EndPoint endpoint = new IPEndPoint(IPAddress.Any, 0);

                byte[] recvdata = new byte[8192];
                this.connectionListener.ReceiveFrom(recvdata, ref endpoint);
                ReceiveMessage recmessage = new ReceiveMessage(recvdata);
                IPEndPoint ip = (IPEndPoint)endpoint;
                foreach(Connection conn in this.users)
                {
                    if(conn.IPAddress.Equals(ip.Address) && conn.Port == ip.Port)
                    {
                        conn.AddMessage(recmessage);
                        goto end;
                    }
                }
                Client nclient= new Client("testclient", ip.Address, ip.Port);
                AddUser(nclient);
                nclient.AddMessage(recmessage);
                end:
                continue;
            }

        }

        public void Stop()
        {
            if(udpSocket != null)
                this.udpSocket.Close();
            if (this.connectionListener != null)
                this.connectionListener.Close();
            if(listenerActive)
                this.listenerThread.Abort();
            this.listenerActive = false;
        }
    }
}
