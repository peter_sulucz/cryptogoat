﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CryptoGoat.Com;
using CryptoGoat.Users;
using System.Net;

namespace Dcntrl
{
    /// <summary>
    /// Interaction logic for UserListItem.xaml
    /// </summary>
    public partial class AddUserListItem : UserControl
    {
        private IPAddress address;
        private int? port;
        private CommunicationManager manager;
        public AddUserListItem(CommunicationManager manager)
        {
            InitializeComponent();
            this.manager = manager;
        }

        private void IPBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                address = IPAddress.Parse(IPBox.Text);
                IPBox.Background = Brushes.LightGreen;

            }
            catch(FormatException)
            {
                IPBox.Background = Brushes.LightPink;
                address = null;
            }
            addReady();
        }

        private void addReady()
        {
            if (port != null && this.address != null)
                Addbutton.IsEnabled = true;
            else
                Addbutton.IsEnabled = false;
        }

        private void PortBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                port = int.Parse(PortBox.Text);
                PortBox.Background = Brushes.LightGreen;
            }
            catch(FormatException)
            {
                PortBox.Background = Brushes.LightPink;
                port = null;
            }
            addReady();
        }

        private void Addbutton_Click(object sender, RoutedEventArgs e)
        {
            Client user = new Client("Friend", address, port.Value);
            this.manager.AddUser(user);
            this.Background = Brushes.LightBlue;
            this.PortBox.Clear();
            this.address = null;
            this.port = null;
            this.IPBox.Clear();
        }
    }
}
