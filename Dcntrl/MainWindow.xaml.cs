﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using CryptoGoat.Com;

namespace Dcntrl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MaskedTextProvider numberMask;
        private CommunicationManager comManager;
        bool running = false;

        public MainWindow()
        {
            InitializeComponent();
            this.comManager = new CommunicationManager();
            this.comManager.OnNewMessage += comManager_OnNewMessage;
            this.comManager.OnNewUser += comManager_OnNewUser;
            this.Closing += MainWindow_Closing;
            this.setupMasks();

            AddUserListItem userbox = new AddUserListItem(this.comManager);
            ClientBox.Items.Add(userbox);
        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            this.comManager.Stop();
        }

        void comManager_OnNewUser(CryptoGoat.Users.Connection user)
        {
            
            Application.Current.Dispatcher.Invoke((Action)delegate
            {

                UserListItem item = new UserListItem(this.comManager, user);
                this.ClientBox.Items.Add(item);
            });
            
        }

        void comManager_OnNewMessage(Message message)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                System.Windows.Threading.DispatcherPriority.Normal,
                (Action)delegate()
                {
                    this.RawConvo.Items.Add(message);
                });
            
        }

        private void setupMasks()
        {
            this.numberMask = new MaskedTextProvider("#####");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(running)
            {
                this.comManager.Stop();
                StartButton.Content = "Start";
            }
            else
            {
                int port = int.Parse(PortBox.Text);
                this.comManager.Start(port);
                StartButton.Content = "Stop";
            }
            this.running = !this.running;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!this.numberMask.VerifyString(e.Text))
                e.Handled = true;
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(SendText.Text);
            this.comManager.BroadcastAll(data);
        }
    }
}
