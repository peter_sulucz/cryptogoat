﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CryptoGoat.Com;
using CryptoGoat.Users;
using System.Net;

namespace Dcntrl
{
    /// <summary>
    /// Interaction logic for UserListItem.xaml
    /// </summary>
    public partial class UserListItem : UserControl
    {
        private Connection user;
        private CommunicationManager manager;
        public UserListItem(CommunicationManager manager, Connection user)
        {
            InitializeComponent();
            this.manager = manager;
            this.user = user;
            this.UserName.Content = user.Name;
        }
    }
}
